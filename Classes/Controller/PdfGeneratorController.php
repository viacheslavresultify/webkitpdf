<?php

namespace Pixelant\Webkitpdf\Controller;

use Pixelant\Webkitpdf\Exceptions\InvalidFolderOutputPathException;
use Pixelant\Webkitpdf\Exceptions\InvalidScriptPathException;
use Pixelant\Webkitpdf\Exceptions\InvalidUrlParameterException;
use Pixelant\Webkitpdf\Utility\PdfUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;

/**
 * Class PdfGeneratorController
 */
class PdfGeneratorController extends ActionController
{
    /**
     * Cache Manager
     *
     * @var \Pixelant\Webkitpdf\Cache\CacheManager
     * @inject
     */
    protected $cacheManager;

    /**
     * Main pdf method
     */
    public function generateAction()
    {
        // get required stuff
        list($url, $downloadName) = $this->getPdfUrlAndDownloadNameFromRequest();

        // check if user is logged in and add FE use session link
        if (PdfUtility::isFrontendLoggin()) {
            $url = PdfUtility::appendFESessionInfoToURL($url);
        }

        // security check
        $url = PdfUtility::sanitizeURL(
            $url,
            GeneralUtility::trimExplode(
                ',',
                $this->settings['allowedHosts'] ?? '',
                true
            )
        );
        $url = PdfUtility::decodeUrl($url);

        if (!$this->cacheManager->isInCache($url)
            || PdfUtility::getConfiguration()['debug'] === '1'
            || PdfUtility::isFrontendLoggin()
        ) {
            $targetFile = $this->determinateTargetFilePath($downloadName);

            $scriptCall = $this->determinateScriptPath() .
                $this->buildScriptOptions() . ' ' .
                $url . ' ' .
                $targetFile .
                ' 2>&1';

            exec($scriptCall, $output);

            // Write debugging information to devLog
            PdfUtility::debugLogging('Executed shell command', -1, [$scriptCall]);
            PdfUtility::debugLogging('Output of shell command', -1, [$output]);


            $this->cacheManager->store($url, $targetFile);
        } else {
            //read file path from cache
            $cacheEntry = $this->cacheManager->getFromCache($url);
            $targetFile = $cacheEntry->getFilePath();
        }

        $this->outputPdf($targetFile);
    }

    /**
     * Ouput generated PDF
     *
     * @param $filePath
     */
    protected function outputPdf($filePath)
    {

        $fileInfo = new \SplFileInfo($filePath);
        $contentDisposition = ($this->settings['contentDisposition'] !== 'attachment' ? 'inline' : 'attachment')
            . '; filename="' . $fileInfo->getFilename() . '"';

        $fileSize = filesize($filePath);

        header('Content-Type: application/pdf');
        header('Content-Length: ' . $fileSize);
        header('Content-Disposition: ' . $contentDisposition);
        header('X-Robots-Tag: noindex');
        readfile($filePath);

        if (!$this->cacheManager->isEnabled()) {
            unlink($filePath);
        }

        exit(0);
    }

    /**
     * Get script path
     *
     * @return string
     * @throws InvalidScriptPathException
     */
    protected function determinateScriptPath()
    {
        $targetFolder = GeneralUtility::getFileAbsFileName(
            $this->settings['scriptPath']
        );
        $file = PdfUtility::sanitizePath($targetFolder) . 'wkhtmltopdf';

        if (!is_file($file) || !is_executable($file)) {
            throw new InvalidScriptPathException($file);
        }

        return $file;
    }

    /**
     * Folder where to save temp pdf files
     *
     * @param string $fileName
     * @return string
     * @throws InvalidFolderOutputPathException
     * @throws \Exception
     */
    protected function determinateTargetFilePath(string $fileName)
    {
        $folder = GeneralUtility::getFileAbsFileName(
            PdfUtility::sanitizePath($this->settings['customTempOutputPath'] ?: 'typo3temp/tx_webkitpdf')
        );

        // check if it exist
        if (!file_exists($folder)) {
            // try to create it
            GeneralUtility::mkdir($folder);
        }

        if (is_dir($folder) && is_writable($folder)) {
            $append = '';
            do {
                $fileInfo = new \SplFileInfo($fileName);
                $baseName = $fileInfo->getBasename('.' . $fileInfo->getExtension());
                $fileTarget = $folder . $baseName .
                    ((int)$append > 0 ? '_' : '') . $append . '.' . $fileInfo->getExtension();

                $append = (int)$append + 1;
                if ($append > 1000) {
                    throw new \Exception('Something went wrong while creating PDF file', 1503406688277);
                }
            } while (file_exists($fileTarget));

            return $fileTarget;
        } else {
            throw new InvalidFolderOutputPathException($folder);
        }
    }

    /**
     * Get url and file name from request
     *
     * @return array
     * @throws InvalidUrlParameterException
     */
    protected function getPdfUrlAndDownloadNameFromRequest()
    {
        // If no argument - we get exception
        try {
            $url = $this->request->getArgument('url');
        } catch (NoSuchArgumentException $e) {
            $url = '';
        }

        if (empty($url)) {
            throw new InvalidUrlParameterException($url);
        }
        try {
            $downloadName = $this->request->getArgument('downloadName');
        } catch (NoSuchArgumentException $e) {
            $downloadName = $this->settings['defaultDownloadName'];
        }

        $downloadName = preg_replace(
            '/[^A-Za-z0-9_.\-]/',
            '',
            str_replace(' ', '_', $downloadName) . '.pdf'
        );

        return [$url, $downloadName];
    }

    /**
     * Read script settings
     *
     * @return array
     */
    protected function readScriptSettings()
    {
        $tsSettings = $this->settings['scriptParams'] ?? [];

        $finalSettings = array();
        foreach ($tsSettings as $param => $value) {
            $value = trim($value);
            if (substr($param, 0, 2) !== '--') {
                $param = '--' . $param;
            }
            $finalSettings[$param] = $value;
        }

        return $finalSettings;
    }

    /**
     * Creates the parameters for the wkhtmltopdf call.
     *
     * @return string The parameter string
     */
    protected function buildScriptOptions()
    {
        $options = [];
        if ($this->settings['pdfOptions']['pageURLInHeader']) {
            $options['--header-center'] = '[webpage]';
        }

        if ($this->settings['pdfOptions']['copyrightNotice']) {
            $options['--footer-left'] = '© ' . date('Y', time()) . $this->settings['pdfOptions']['copyrightNotice'];
        }

        if ($this->settings['pdfOptions']['additionalStylesheet']) {
            $additionalStylesheet = '/' . ltrim($this->settings['pdfOptions']['additionalStylesheet'], '/');
            $options['--user-style-sheet'] = GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . $additionalStylesheet;
        }

        $options = array_merge($options, $this->readScriptSettings());

        $paramsString = '';
        foreach ($options as $param => $value) {
            if (!empty($value)) {
                $value = '"' . $value . '"';
            }
            $paramsString .= ' ' . $param . ' ' . $value;
        }

        return $paramsString;
    }
}
