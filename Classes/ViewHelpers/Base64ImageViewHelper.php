<?php
declare(strict_types=1);

namespace Pixelant\Webkitpdf\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper;

/**
 * Class Base64ImageViewHelper
 * @package Pixelant\Webkitpdf\ViewHelpers
 */
class Base64ImageViewHelper extends ImageViewHelper
{
    /**
     * Renders image tag as base64 encoded
     *
     * @return string
     */
    public function render()
    {
        // Reset absolute path, since no sense for base64
        if ($this->arguments['absolute']) {
            $this->arguments['absolute'] = false;
        }
        $tag = parent::render();

        if (!empty($tag)) {
            $doc = new \DOMDocument();
            $doc->loadHTML($tag, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $doc->getElementsByTagName('img');

            /** @var \DOMElement $image */
            foreach ($images as $image) {
                $src = $image->getAttribute('src');
                $path = PATH_site . '/' . ltrim($src, '/');

                if (is_readable($path)) {
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);

                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image->setAttribute('src', $base64);
                    unset($base64, $data);

                    return $doc->saveHTML();
                }
            }
        }

        return $tag;
    }
}
