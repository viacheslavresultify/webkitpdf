<?php

namespace Pixelant\Webkitpdf\Exceptions;

/**
 * Class NoAllowedHostException
 * @package Pixelant\Webkitpdf\Exceptions
 */
class InvalidScriptPathException extends \Exception
{
    /**
     * @param string $scriptPath
     */
    public function __construct($scriptPath)
    {
        parent::__construct(
            sprintf(
                'Script ("%s") could not be executed or doesn\'t exist.',
                $scriptPath
            ),
            1503401918425
        );
    }
}
