<?php

namespace Pixelant\Webkitpdf\Exceptions;

/**
 * Class InvalidFolderOutputPathException
 * @package Pixelant\Webkitpdf\Exceptions
 */
class InvalidFolderOutputPathException extends \Exception
{
    /**
     * @param string $scriptPath
     */
    public function __construct($folderPath)
    {
        parent::__construct(
            sprintf(
                'Folder ("%s") doesn\'t exist or is not writable.',
                $folderPath
            ),
            1503402771268
        );
    }
}
