<?php

namespace Pixelant\Webkitpdf\Exceptions;

/**
 * Class NoAllowedHostException
 * @package Pixelant\Webkitpdf\Exceptions
 */
class NotAllowedHostException extends \Exception
{
    /**
     * NoAllowedHostException constructor.
     * @param string $host
     */
    public function __construct($host)
    {
        parent::__construct(
            sprintf(
                'Host ("%s") is not allowed.',
                $host
            ),
            1503397726239
        );
    }
}
