<?php

namespace Pixelant\Webkitpdf\Exceptions;

/**
 * Class InvalidUrlParameterException
 * @package Pixelant\Webkitpdf\Exceptions
 */
class InvalidUrlParameterException extends \Exception
{
    /**
     * InvalidUrlParameterException constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        parent::__construct(
            sprintf(
                'Bad "url" parameter ("%s") was detected by webkitpdf. Can not be empty.',
                $url
            ),
            1503389928806
        );
    }
}
