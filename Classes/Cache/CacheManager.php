<?php
declare(strict_types=1);

namespace Pixelant\Webkitpdf\Cache;

use Pixelant\Webkitpdf\Domain\Model\CacheEntry;
use Pixelant\Webkitpdf\Utility\PdfUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class CacheManager
 * @package Pixelant\Webkitpdf\Cache
 */
class CacheManager
{
    /**
     * @var \Pixelant\Webkitpdf\Domain\Repository\CacheEntryRepository
     * @inject
     */
    protected $cacheEntryRepository;

    /**
     * Extension configuration
     *
     * @var array
     */
    protected $extConf = [];

    /**
     * Is cache enabled
     *
     * @var bool
     */
    protected $isEnabled = true;

    /**
     * CacheManager constructor.
     */
    public function __construct()
    {
        $this->extConf = PdfUtility::getConfiguration();

        if (intval($this->extConf['cacheThreshold']) === 0 || intval($this->extConf['disableCache'])) {
            $this->isEnabled = false;
        }
    }

    /**
     * Check if there is an cache entry
     *
     * @param string $url
     * @return bool
     */
    public function isInCache(string $url): bool
    {
        if ($this->isEnabled()) {
            return $this->getFromCache($url) !== null;
        }

        return false;
    }

    /**
     * @param $url
     * @return CacheEntry|null
     */
    public function getFromCache(string $url)
    {
        /** @var CacheEntry $cacheEntry */
        $cacheEntry = $this->cacheEntryRepository->findCacheByUrlHash(md5($url));

        if ($cacheEntry !== null
            && (!file_exists($cacheEntry->getFilePath())
            || $this->isCacheEntryExpired($cacheEntry))
        ) {
            $this->cacheEntryRepository->fullRemoveFromDB($cacheEntry);
            return null;
        }

        return $cacheEntry;
    }

    /**
     * Save cache entry
     *
     * @param string $url
     * @param string $filePath
     */
    public function store(string $url, string $filePath)
    {
        if ($this->isEnabled() && file_exists($filePath)) {
            /** @var CacheEntry $cacheEntry */
            $cacheEntry = GeneralUtility::makeInstance(CacheEntry::class);

            $cacheEntry->setUrlHash(md5($url));
            $cacheEntry->setFilePath($filePath);

            $this->cacheEntryRepository->add($cacheEntry);
            GeneralUtility::makeInstance(PersistenceManager::class)->persistAll();
        }
    }

    /**
     * Check is cache entry expire
     *
     * @param CacheEntry $cacheEntry
     * @return bool
     */
    public function isCacheEntryExpired(CacheEntry $cacheEntry)
    {
        return $cacheEntry->getCrdate() < PdfUtility::getCacheExpireTime();
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }
}
