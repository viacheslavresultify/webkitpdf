<?php
declare(strict_types=1);

namespace Pixelant\Webkitpdf\Hooks;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ClearCacheHook
 * @package Pixelant\Webkitpdf\Hooks
 */
class ClearCacheHook
{
    /**
     * Remove outdate entries on clear cache
     *
     * @param array $params
     * @param DataHandler $pObj
     */
    public function clearCacheCmd(
        array $params,
        /** @noinspection PhpUnusedParameterInspection */ DataHandler $pObj
    ) {
        if (GeneralUtility::inList('all,pages', $params['cacheCmd'])) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
                'tx_webkitpdf_domain_model_cacheentry'
            );
            $uids = [];

            $rawCache = $queryBuilder
                ->select('uid', 'file_path')
                ->from('tx_webkitpdf_domain_model_cacheentry')
                ->execute();

            while ($cacheEntry = $rawCache->fetch()) {
                $uids[] = $cacheEntry['uid'];
                if (file_exists($cacheEntry['file_path'])) {
                    unlink($cacheEntry['file_path']);
                }
            }

            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
                'tx_webkitpdf_domain_model_cacheentry'
            );

            $queryBuilder
                ->delete('tx_webkitpdf_domain_model_cacheentry')
                ->where(
                    $queryBuilder->expr()->in(
                        'uid',
                        $queryBuilder->createNamedParameter(
                            $uids,
                            Connection::PARAM_INT_ARRAY
                        )
                    )
                )
                ->execute();
        }
    }
}
