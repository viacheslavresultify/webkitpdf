<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "webkitpdf".
 *
 * Auto generated 19-05-2014 12:15
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Webkit PDFs',
    'description' => 'Generate PDF files using WebKit rendering engine.',
    'category' => 'plugin',
    'version' => '2.2.0',
    'state' => 'stable',
    'createDirs' => 'typo3temp/tx_webkitpdf',
    'author' => 'Dev-Team Typoheads',
    'author_email' => 'dev@typoheads.at',
    'author_company' => 'Typoheads GmbH',
    'constraints' =>
        [
            'depends' => [
                'typo3' => '8.0.0-9.5.99'
            ],
            'conflicts' => [],
            'suggests' => [],
        ]
);
