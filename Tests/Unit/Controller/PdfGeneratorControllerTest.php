<?php

namespace Pixelant\Webkitpdf\Tests\Controller;

use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pixelant\Webkitpdf\Cache\CacheManager;
use Pixelant\Webkitpdf\Controller\PdfGeneratorController;
use Pixelant\Webkitpdf\Domain\Model\CacheEntry;
use Pixelant\Webkitpdf\Exceptions\InvalidScriptPathException;
use Pixelant\Webkitpdf\Exceptions\InvalidUrlParameterException;
use Pixelant\Webkitpdf\Utility\PdfUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class PdfGeneratorControllerTest extends UnitTestCase
{
    /**
     * @var \Pixelant\Webkitpdf\Cache\CacheManager|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $cacheManager;

    protected $tsfe;

    public function setUp()
    {
        // Set extension settings
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf'] = serialize($this->getExtConfiguration());

        $this->cacheManager = $this->getMockBuilder(CacheManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getFromCache', 'store', 'isInCache'])
            ->getMock();

        $this->tsfe = $this->createMock(TypoScriptFrontendController::class);

        $GLOBALS['TSFE'] = $this->tsfe;
    }

    /**
     * @test
     */
    public function cacheEntryFoundWillLoadPdfFromCache()
    {
        $url = 'http://test.com/';
        $urlSanitize = PdfUtility::wrapUriName($url);

        $downloadName = 'Pdf file';
        $tempFile = 'webkitpdf_unit_test.pdf';

        $cacheEntry = new CacheEntry();
        $cacheEntry->setFilePath($tempFile);

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['getPdfUrlAndDownloadNameFromRequest', 'outputPdf']
        );

        $moc->_set('cacheManager', $this->cacheManager);
        $moc->_set('settings', ['allowedHosts' => 'test.com']);

        $moc->expects($this->once())
            ->method('getPdfUrlAndDownloadNameFromRequest')
            ->willReturn([$url, $downloadName]);

        $moc->expects($this->once())
            ->method('outputPdf')
            ->with($tempFile);

        $this->cacheManager->expects($this->once())
            ->method('isInCache')
            ->with($urlSanitize)
            ->willReturn(true);

        $this->cacheManager->expects($this->once())
            ->method('getFromCache')
            ->with($urlSanitize)
            ->willReturn($cacheEntry);

        $moc->generateAction();
    }

    /**
     * @test
     */
    public function wrongScriptPathWillThrowException()
    {
        $settings = [
            'scriptPath' => 'invalid_path'
        ];

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );

        $moc->_set('settings', $settings);

        $this->expectException(InvalidScriptPathException::class);
        $moc->_call('determinateScriptPath');
    }

    /**
     * @test
     */
    public function correctScriptPathWillResturnValidScriptPath()
    {
        $path = ExtensionManagementUtility::extPath('webkitpdf', 'Resources/Private/Shell/');
        $scriptName = 'wkhtmltopdf';

        $settings = [
            'scriptPath' => $path
        ];

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );

        $moc->_set('settings', $settings);

        $result = $moc->_call('determinateScriptPath');

        $this->assertEquals(
            $path . $scriptName,
            $result
        );
    }

    /**
     * @test
     */
    public function ifFileExistItWillAppendItWithCountableNumberOrCreateFile()
    {
        $fileName = 'pdf_file.pdf';
        $path = PATH_site . 'typo3temp/tx_webkitpdf/';

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );

        $result1 = $moc->_call('determinateTargetFilePath', $fileName);
        touch($result1);
        $result2 = $moc->_call('determinateTargetFilePath', $fileName);
        touch($result2);
        $result3 = $moc->_call('determinateTargetFilePath', $fileName);

        unlink($result1);
        unlink($result2);

        $this->assertEquals(
            $path . $fileName,
            $result1
        );

        $this->assertEquals(
            $path . 'pdf_file_1.pdf',
            $result2
        );

        $this->assertEquals(
            $path . 'pdf_file_2.pdf',
            $result3
        );
    }

    /**
     * @test
     */
    public function invalidUrlParameterWillThrowException()
    {
        $request = $this->getMockBuilder(Request::class)->setMethods(['getArgument'])->getMock();
        $request->expects($this->once())
            ->method('getArgument')
            ->with('url')
            ->willReturn('');

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );
        $moc->_set('request', $request);

        $this->expectException(InvalidUrlParameterException::class);
        $moc->_call('getPdfUrlAndDownloadNameFromRequest');
    }

    /**
     * @test
     */
    public function correctGetParameterUrlAndDownloadNameWillValidResult()
    {
        $arguments = [
            'downloadName' => 'Pdf file',
            'url' => 'http://test.com/'
        ];
        $expect = [
            'http://test.com/',
            'Pdf_file.pdf'
        ];

        $request = new Request();
        $request->setArguments($arguments);

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );
        $moc->_set('request', $request);

        $this->assertEquals(
            $expect,
            $moc->_call('getPdfUrlAndDownloadNameFromRequest')
        );
    }

    /**
     * @test
     */
    public function readScriptOptionsCreateFormatedCommandLineParameters()
    {
        $settings = [
            'scriptParams' => [
                'margin-right' => '10mm',
                '--margin-left' => '10mm',
                'disable-smart-shrinking' => ''
            ]
        ];
        $expectLine = ' --margin-right "10mm" --margin-left "10mm" --disable-smart-shrinking ';

        $moc = $this->getAccessibleMock(
            PdfGeneratorController::class,
            ['dummy']
        );
        $moc->_set('settings', $settings);

        $this->assertEquals(
            $expectLine,
            $moc->_call('buildScriptOptions')
        );
    }

    /**
     * Extension settings
     *
     * @return array
     */
    protected function getExtConfiguration()
    {
        return [
            'cacheThreshold' => 10,
            'disableCache' => 0
        ];
    }

    public function tearDown()
    {
        unset($GLOBALS['TSFE']);

        unset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf']);

        $reflection = new \ReflectionProperty(PdfUtility::class, 'configuration');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);
    }
}
