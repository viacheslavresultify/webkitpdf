<?php
namespace Pixelant\Webkitpdf\Tests\Domain\Model;

use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pixelant\Webkitpdf\Domain\Model\CacheEntry;

/**
 * Class CacheEntryTest
 * @package Pixelant\Webkitpdf\Tests\Domain\Model
 */
class CacheEntryTest extends UnitTestCase
{
    /**
     * @var CacheEntry
     */
    protected $fixture;

    public function setUp()
    {
        $this->fixture = new CacheEntry();
    }

    public function tearDown()
    {
        unset($this->fixture);
    }

    /**
     * @test
     */
    public function urlHashInitializeValueEmptyString()
    {
        $this->assertEquals(
            '',
            $this->fixture->getUrlHash()
        );
    }

    /**
     * @test
     */
    public function urlHashCanBeSet()
    {
        $value = 'urlHash';

        $this->fixture->setUrlHash($value);

        $this->assertEquals(
            $value,
            $this->fixture->getUrlHash()
        );
    }

    /**
     * @test
     */
    public function filePathInitializeValueEmptyString()
    {
        $this->assertEquals(
            '',
            $this->fixture->getFilePath()
        );
    }

    /**
     * @test
     */
    public function filePathCanBeSet()
    {
        $filePath = 'path_to_file';

        $this->fixture->setFilePath($filePath);

        $this->assertEquals(
            $filePath,
            $this->fixture->getFilePath()
        );
    }

    /**
     * @test
     */
    public function crDateCanBeSet()
    {
        $date = 1503918156398;
        $this->fixture->setCrdate($date);

        $this->assertSame(
            $date,
            $this->fixture->getCrdate()
        );
    }
}
