<?php

namespace Pixelant\Webkitpdf\Tests\Cache;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pixelant\Webkitpdf\Cache\CacheManager;
use Pixelant\Webkitpdf\Domain\Model\CacheEntry;
use Pixelant\Webkitpdf\Domain\Repository\CacheEntryRepository;
use Pixelant\Webkitpdf\Utility\PdfUtility;

/**
 * Class CacheManager
 * @package Pixelant\Webkitpdf\Cache
 */
class CacheManagerTest extends UnitTestCase
{
    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AccessibleMockObjectInterface|CacheEntryRepository
     */
    protected $cacheEntryRepository;

    public function setUp()
    {
        // Set extension settings
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf'] = serialize($this->getExtConfiguration());

        $this->cacheEntryRepository = $this->getMockBuilder(CacheEntryRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findCacheByUrlHash', 'fullRemoveFromDB', 'add'])
            ->getMock();

        $this->cacheManager = $this->getAccessibleMock(
            CacheManager::class,
            ['dummy']
        );

        $this->cacheManager->_set('cacheEntryRepository', $this->cacheEntryRepository);
    }

    /**
     * @test
     */
    public function cacheIsEnableByDefault()
    {
        $this->assertTrue($this->cacheManager->isEnabled());
    }

    /**
     * @test
     */
    public function cacheEntryFoundButFileDoesntExistWillRemoveEntryAndReturnNull()
    {
        $url = 'http://test.com/';
        $cacheEntry = new CacheEntry();

        $this->cacheEntryRepository->expects($this->once())
            ->method('findCacheByUrlHash')
            ->with(md5($url))
            ->willReturn($cacheEntry);

        $this->cacheEntryRepository->expects($this->once())
            ->method('fullRemoveFromDB')
            ->with($cacheEntry);

        $result = $this->cacheManager->getFromCache($url);

        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function cacheEntryExpireCheckCreationDate()
    {
        $cacheEntry = $this->getMockBuilder(CacheEntry::class)
                        ->setMethods(['getCrdate'])
                        ->getMock();
        $cacheEntry->expects($this->once())
            ->method('getCrdate')
            ->willReturn(time());

        $this->cacheManager->isCacheEntryExpired($cacheEntry);
    }

    /**
     * Extension settings
     *
     * @return array
     */
    protected function getExtConfiguration()
    {
        return [
            'cacheThreshold' => 10,
            'disableCache' => 0
        ];
    }

    public function tearDown()
    {
        unset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf']);

        $reflection = new \ReflectionProperty(PdfUtility::class, 'configuration');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);
    }
}
