<?php
defined('TYPO3_MODE') || die('access denied');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Pixelant.' . $_EXTKEY,
    'Pdf',
    [
        'PdfGenerator' => 'generate'
    ],
    [
        'PdfGenerator' => 'generate'
    ]
);

// Clear_cache
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][] =
    \Pixelant\Webkitpdf\Hooks\ClearCacheHook::class . '->clearCacheCmd';
