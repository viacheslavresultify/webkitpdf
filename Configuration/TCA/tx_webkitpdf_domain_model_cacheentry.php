<?php
defined('TYPO3_MODE') or die('Access denied.');

$ll = 'LLL:EXT:webkitpdf/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_webkitpdf_domain_model_cache_entry',
        'label' => 'file_name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],

        'searchFields' => 'file_path',
        'hideTable' => 1
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, url_hash, file_path',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, url_hash, file_path'],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'file_path' => [
            'exclude' => 1,
            'label' => $ll . 'tca.file_path',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'url_hash' => [
            'exclude' => 1,
            'label' => $ll . 'tca.url_hash',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'crdate' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ]
    ]
];
