<?php
defined('TYPO3_MODE') || die('Access denied.');

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['webkitpdf_pdf'] =
    'pages,recursive,layout,select_key';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'webkitpdf',
    'Pdf',
    'WebKit PDF'
);
