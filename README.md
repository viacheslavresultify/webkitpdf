# Pixelant
## Webkitpdf

### How to setup PDF generator ?

* Install extension
* Include TypoScript template of extension
* Create new page for PDF plugin
* Go to new page and add a **Webkitpdf** plugin on a page
* Set plugin Pid constant **"plugin.tx_webkitpdf_pdf.pdfLink.pluginPid"** which is newly created page.
* Add link to your page using TypoScript lib. 
```
page.99 < lib.tx_webkitpdf_pdf.pdfLink
```

### Example of link created in fluid for product single view 
```
<f:link.action
 action="generate"
 pluginName="Pdf"
 extensionName="Webkitpdf"
 controller="PdfGenerator"
 pageUid="{settings.printPage.pluginPid}"
 arguments="{downloadName: '{product.name}_{product.sku}', url: '{f:uri.page(absolute: 1, addQueryString: 1)}'}">
       Pdf link                                     
</f:link.action>
```

## Installation

To install tooling dependencies, run:

    npm install

Then install the composer dependencies:

    composer install

## Workflow

### Test

For working with the extension, the following can be run to accomplish common tasks.

To run the PHP codesniffer run the following command:

    npm run php:codesniffer

To run the PHP Unit tests run the following command:

    npm run php:unittests

To simulate the full build process locally, then run this packaged command:

    npm run build:suite --silent